amoCRM form for Drupal is a module designed to generate leads, contacts and
company information directly in your amoCRM account.

FEATURES
--------
* Simple Set up: just copy/paste the shortcode.
  No HTML and no manual/programmers are needed;
* Friendly UI: you can select fields you would like to display on your landing
  page and adjust titles and required options for each field;
* Each time form is submitted all the information
  (leads and contacts) are synchronized with your amoCRM account;
* amoCRM reminds you to contact every lead creating tasks automatically;
* Your website and form can be integrated with Google.Analytics.
  When a lead is converted into a sale, amoCRM automatically sends
  information to your Google.Analytics accounts.
* Receive real-time feedback, reports and true comprehensive
  view of your sales funnel.

INSTALLATION
------------
* Install as usual, see #70151 for further information;
* Enable "Convert amoCRM short-code to form" filter
  for necessary text formats (admin/config/content/formats);
* Move "Convert amocrm short-code to form" filter lower then
  "Limit allowed HTML tags" filter.

USAGE
-----
Go to the Lead page in your amoCRM account;
Click on "More" button and then on "Add form" button;
Adjust a form as you need;
At the bottom of the page a shortcode for Drupal will appear;
Copy and paste it in the necessary content field in Drupal.
Installation and using demo

AUTHORS
-------
Dmitry Kiselev (kala4ek) - https://www.drupal.org/u/kala4ek
